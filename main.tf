provider "aws" {
  alias = "east"
  region = "us-east-1"
}

resource "aws_s3_bucket" "devops-msrz85" {
  bucket = "msrz85-s3-bucket"

  tags = {
    Name        = "MSRZ85 Test Website"
    Environment = "Dev"
  }
}

terraform {
  backend "s3" {
    bucket = "msrz85-s3-bucket"
    key = "terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-locks-deviget-2022"
    encrypt = true
  }
}

resource "aws_dynamodb_table" "terraform-locks" {
  name         = "terraform-locks-deviget-2022"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_s3_bucket_versioning" "versioning_devops-msrz85" {
  bucket = aws_s3_bucket.devops-msrz85.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_acl" "devops-msrz85-acl" {
  bucket = aws_s3_bucket.devops-msrz85.id
  acl    = "public-read"
}

# Create bucket policy
resource "aws_s3_bucket_policy" "devops-msrz85-policy" {
  bucket = aws_s3_bucket.devops-msrz85.id

  policy = <<POLICY
  {
      "Version": "2012-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Principal": "*",
              "Action": "s3:GetObject",
              "Resource": "${aws_s3_bucket.devops-msrz85.arn}/*"
          }
      ]
  }
  POLICY
}

resource "aws_s3_bucket_public_access_block" "devops-msrz85" {
  bucket = aws_s3_bucket.devops-msrz85.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_kms_key" "devops-msrz85-key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}

resource "aws_s3_object" "object" {
  bucket = "msrz85-s3-bucket"
  key    = "index.html"
  content_type = "text/html"
  source = "./site/index.html"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  etag = filemd5("./site/index.html")
}

variable "mime_types" {
  default = {
    htm   = "text/html"
    html  = "text/html"
    css   = "text/css"
    js    = "application/javascript"
    map   = "application/javascript"
    json  = "application/json"
    png   = "image/png"
    jpg   = "image/jpg"
    jpeg  = "image/jpg"
    gif   = "image/gif"
    svg   = "image/svg+xml"
  }
}

resource "aws_s3_bucket_website_configuration" "devops-msrz85" {
  bucket = aws_s3_bucket.devops-msrz85.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }
}